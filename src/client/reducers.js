import { combineReducers } from 'redux';

import { reducer as transactions } from './components/transactionsList';

export default combineReducers({
  transactions,
});
