import axios from 'axios';

import { API_ENDPOINT } from '../constants/appConstants';

const instance = axios.create({
  baseURL: API_ENDPOINT,
});

export const fetchTransactionsService = () =>
  instance.get('/account/transactions')
    .then((response) => response.data);
