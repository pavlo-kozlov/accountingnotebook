import React, {Component} from 'react';

import TransactionsList from './components/transactionsList';

/**
 * Application class that includes React components.
 */
class App extends Component {
  /**
   * @returns {XML} The application component.
   */
  render() {
    return (
      <div>
          <TransactionsList/>
      </div>
    );
  }
}

export default App;
