import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  fetchTransactions,
  selectTransaction,
} from '../transactionsList.actions';
import TransactionsList from './TransactionsList';

class TransactionsListContainer extends Component {
  componentDidMount() {
    this.props.fetchTransactions();
  }

  render() {
    const { transactions } = this.props;

    return (
      <TransactionsList {...{
        transactions,
        onSelect: this.props.selectTransaction,
      }}/>
    );
  }
}

const mapStateToProps = (state) => ({
  transactions: state.transactions,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchTransactions,
  selectTransaction,
}, dispatch);

TransactionsListContainer.propTypes = {
  fetchTransactions: PropTypes.func.isRequired,
  selectTransaction: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsListContainer);
