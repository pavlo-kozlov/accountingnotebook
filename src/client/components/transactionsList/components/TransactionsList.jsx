import React from 'react';
import PropTypes from 'prop-types';

import Transaction from '../../transaction';

const TransactionsList = ({ transactions, onSelect }) => (
  <div>
    {
      transactions.map((transaction) => (
        <Transaction
          key={transaction.id}
          {...{
            transaction,
            onSelect,
          }}
        />
      ))
    }
  </div>
);

TransactionsList.propTypes = {
  transactions: PropTypes.arrayOf(
    PropTypes.shape({}),
  ),
  onSelect: PropTypes.func.isRequired,
};

export default TransactionsList;
