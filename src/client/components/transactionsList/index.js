import TransactionsList from './components/TransactionsList.container';
import reducer from './transactionsList.reducer';

export {
  TransactionsList as default,
  reducer,
};
