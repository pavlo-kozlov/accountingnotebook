import {
  SELECT_TRANSACTION,
  TRANSACTIONS_LOADED,
} from './transactionsList.actionTypes';

const reducer  = (state = [], action) => {
  let newState = state;
  switch (action.type) {
    case SELECT_TRANSACTION:
      newState = state.map((transaction) => ({
          ...transaction,
          isSelected: transaction.id === action.transactionId && !transaction.isSelected,
        }));
      break;
    case TRANSACTIONS_LOADED:
      newState = action.transactions;
      break;
    default:
  }
  return newState;
};

export default reducer;
