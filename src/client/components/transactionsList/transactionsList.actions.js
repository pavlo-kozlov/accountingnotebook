import { fetchTransactionsService } from '../../services/accountServices';
import {
  SELECT_TRANSACTION,
  TRANSACTIONS_LOADED,
} from './transactionsList.actionTypes';

export const selectTransaction = (transactionId) => ({
  type: SELECT_TRANSACTION,
  transactionId,
});

const transactionsLoaded = (transactions) => ({
  type: TRANSACTIONS_LOADED,
  transactions,
});

export const fetchTransactions = () =>
  (dispatch) =>
    fetchTransactionsService()
      .then((data) => dispatch(transactionsLoaded(data)))
      .catch((error) => console.error(error));
