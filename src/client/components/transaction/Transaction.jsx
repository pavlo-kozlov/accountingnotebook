import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import TransactionDetails from '../transactionDetails';
import './transaction.scss';

const Transaction = ({ transaction, onSelect }) => {
  const { amount, balance, id, isSelected, type } = transaction;
  const transactionStyles = classNames(
    'transaction',
    {
      'transaction-credit': type === 'credit',
      'transaction-debit': type === 'debit',
      'active': isSelected,
    }
  );

  return (
    <div className={transactionStyles}>
      <div className="transaction-summary" onClick={() => onSelect(id)}>
        <span className="transaction-summary-amount">{amount}</span>
        <span className="transaction-summary-balance">{balance}</span>
      </div>
      {
        isSelected && <TransactionDetails
          {...{
            ...transaction,
          }}
        />
      }
    </div>
  );
};

Transaction.propTypes = {
  transaction: PropTypes.shape({}),
  onSelect: PropTypes.func.isRequired,
};

export default Transaction;
