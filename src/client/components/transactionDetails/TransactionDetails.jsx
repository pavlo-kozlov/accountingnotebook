import React from 'react';
import PropTypes from 'prop-types';

import './transactionDetails.scss';

const TransactionDetails = ({ amount, balance, id, type }) => (
  <div className="transaction-details">
    <table>
      <tbody>
        <tr>
          <th>Id</th>
          <td>{id}</td>
        </tr>
        <tr>
          <th>Type</th>
          <td>{type}</td>
        </tr>
        <tr>
          <th>Amount</th>
          <td>{amount}</td>
        </tr>
        <tr>
          <th>Balance</th>
          <td>{balance}</td>
        </tr>
      </tbody>
    </table>
  </div>
);

TransactionDetails.propTypes = {
  amount: PropTypes.number.isRequired,
  balance: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
};

export default TransactionDetails;
