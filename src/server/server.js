const express = require('express');
const bodyParser = require('body-parser');
const { DEBIT, CREDIT } = require('./constants/transactionConstants');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

const transactions = [];

app.get('/account/transactions', function (req, res) {
	res.send(transactions);
});

app.post('/account/transaction', function (req, res) {
	const { amount, type } = req.body;
	let deltaAmount = 0;
	switch (type) {
		case CREDIT:
			deltaAmount = -amount;
			break;
		case DEBIT:
			deltaAmount = +amount;
			break;
		default:
      res.status(422);
      res.send('Transaction type is not recognized');
      return;
	}

  const balance = transactions.length > 0 ? transactions[transactions.length - 1].balance : 0;
  if (balance + deltaAmount >= 0) {
    transactions.push({
      amount: deltaAmount,
      balance: balance + deltaAmount,
      id: Date.now(),
      type,
    });
    res.status(201);
    res.send(transactions);
  } else {
    res.status(422);
    res.send('You don\'t have enough money to perform this operation');
  }
});

app.listen(3001, () => console.log('Example app listening on port 3001!'));
